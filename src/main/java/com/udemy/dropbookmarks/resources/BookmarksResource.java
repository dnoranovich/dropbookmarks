/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.udemy.dropbookmarks.resources;

import com.google.common.base.Optional;
import com.udemy.dropbookmarks.core.Bookmark;
import com.udemy.dropbookmarks.core.User;
import com.udemy.dropbookmarks.db.BookmarkDAO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * A resource class to manipulate bookmarks.
 *
 * @author Dmitry Noranovich
 */
@Path("/bookmarks")
//https://localhost:8443/bookmarks
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookmarksResource {

    /**
     * DAO that provides CRUD methods.
     */
    private BookmarkDAO dao;

    /**
     * A constructor.
     *
     * @param dao DAO that provides CRUD methods.
     */
    public BookmarksResource(BookmarkDAO dao) {
        this.dao = dao;
    }

    /**
     * Method to produce all bookmarks of the authenticated user.
     *
     * @param user authenticated user.
     * @return list of all bookmarks of the authenticated user.
     */
    @GET
    @UnitOfWork
    public List<Bookmark> getBookmarks(@Auth User user) {
        return dao.findForUser(user.getId());
    }

    /**
     * Method looks for a bookmark and returns it only if it belongs to the
     * authenticated user.
     *
     * @param bookmarkId the id of a bookmark to be found.
     * @param userId the id of the authenticated user.
     * @return an Optional containing the found bookmark or an empty optional
     * otherwise.
     */
    private Optional<Bookmark> findIfAuthorized(long bookmarkId, long userId) {
        Optional<Bookmark> result
                = dao.findById(bookmarkId);
        if (result.isPresent()
                && userId != result.get()
                .getUser().getId()) {
            throw new ForbiddenException("You are not authorized to see this resource.");
        }
        return result;
    }

    /**
     * Method returns a bookmark by id, only if the bookmark belongs to
     * authenticated user.
     *
     * @param id the id of a bookmark.
     * @param user the authenticated user.
     * @return Optional of the bookmark if it belongs to the authenticated user
     * or an empty optional if the bookmark was not found.
     */
    @GET
    @Path("/{id}")
    //https://localhost:8443/bookmarks/1
    @UnitOfWork
    public Optional<Bookmark> getBookmark(@PathParam("id") LongParam id, @Auth User user) {
        return findIfAuthorized(id.get(), user.getId());
    }

    /**
     * Method deletes the bookmark, identified y its id, from the database.
     *
     * @param id the id of a bookmark.
     * @param user the authenticated user.
     * @return returns the data contained in the deleted bookmarks.
     */
    @DELETE
    @Path("/{id}")
    @UnitOfWork
    public Optional<Bookmark> delete(@PathParam("id") LongParam id,
            @Auth User user) {
        Optional<Bookmark> bookmark
                = findIfAuthorized(id.get(), user.getId());
        if (bookmark.isPresent()) {
            dao.delete(bookmark.get());
        }
        return bookmark;
    }

    /**
     * A method to add a new bookmark to the list of bookmarks of the
     * authenticated user.
     *
     * @param bookmark a bookmark to be added.
     * @param user the authenticated user.
     * @return the saved bookmark including the auto-generated id.
     */
    @POST
    @UnitOfWork
    public Bookmark saveBookmark(Bookmark bookmark,
            @Auth User user) {
        bookmark.setUser(user);
        return dao.save(bookmark);
    }

}
