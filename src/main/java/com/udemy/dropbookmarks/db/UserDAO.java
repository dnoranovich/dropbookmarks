/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.udemy.dropbookmarks.db;

import com.google.common.base.Optional;
import com.udemy.dropbookmarks.core.User;
import io.dropwizard.hibernate.AbstractDAO;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 * A DAO class to manipulate users.
 *
 * @author Dmitry Noranovich
 */
public class UserDAO extends AbstractDAO<User> {

    /**
     * A constructor.
     *
     * @param sessionFactory Hibernate session factory.
     */
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * A method to retrieve all users in the database.
     *
     * @return list of all users in the database.
     */
    public List<User> findAll() {
        return list(
                namedQuery(
                        "com.udemy.dropbookmarks.core.User.findAll"
                )
        );
    }

    /**
     * Method looks for a user with particular credentials.
     *
     * @param username username of the user to be found.
     * @param password password of the user to be found.
     * @return an Optional containing found user or an empty Optional if the
     * user was not found.
     */
    public Optional<User> findByUsernamePassword(
            String username,
            String password
    ) {
        return Optional.fromNullable(
                uniqueResult(
                        namedQuery("com.udemy.dropbookmarks.core.User.findByUsernamePassword")
                        .setParameter("username", username)
                        .setParameter("password", password)
                )
        );
    }
}
