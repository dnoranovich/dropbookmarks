/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.udemy.dropbookmarks;

import com.udemy.dropbookmarks.auth.DBAuthenticator;
import com.udemy.dropbookmarks.core.Bookmark;
import com.udemy.dropbookmarks.core.User;
import com.udemy.dropbookmarks.db.BookmarkDAO;
import com.udemy.dropbookmarks.db.UserDAO;
import com.udemy.dropbookmarks.resources.BookmarksResource;
import com.udemy.dropbookmarks.resources.HelloResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFactory;
import io.dropwizard.auth.basic.BasicAuthFactory;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Application class which contains main method.
 *
 * @author Dmitry Noranovich
 */
public class DropBookmarksApplication
        extends Application<DropBookmarksConfiguration> {

    /**
     * Hibernate bundle
     */
    private final HibernateBundle<DropBookmarksConfiguration> hibernateBundle
            = new HibernateBundle<DropBookmarksConfiguration>(User.class,
                    Bookmark.class) {

                @Override
                public DataSourceFactory getDataSourceFactory(
                        DropBookmarksConfiguration configuration
                ) {
                    return configuration.getDataSourceFactory();
                }

            };

    /**
     * main() method of the application.
     *
     * @param args command line arguments
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        new DropBookmarksApplication().run(args);
    }

    @Override
    public String getName() {
        return "DropBookmarks";
    }

    @Override
    public void initialize(final Bootstrap<DropBookmarksConfiguration> bootstrap) {
        //Migrations bundle is added.
        bootstrap.addBundle(new MigrationsBundle<DropBookmarksConfiguration>() {

            @Override
            public DataSourceFactory
                    getDataSourceFactory(DropBookmarksConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        //Hibernate bundle is added.
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(final DropBookmarksConfiguration configuration,
            final Environment environment) {
        //Users DAO is created
        final UserDAO userDAO
                = new UserDAO(hibernateBundle.getSessionFactory());

        //Bookmarks DAO is created
        final BookmarkDAO bookmarkDAO
                = new BookmarkDAO(hibernateBundle
                        .getSessionFactory());

        //The resourse to manipulate bookmarks is registered.
        //DAO is passed as an argument.
        environment.jersey().register(
                new BookmarksResource(bookmarkDAO)
        );

        //A simple resourse returning greeting is registered.
        environment.jersey().register(
                new HelloResource()
        );

        //The Authenicator using database is registered.
        environment.jersey().register(
                AuthFactory.binder(
                        new BasicAuthFactory<>(
                                new DBAuthenticator(userDAO),
                                "SECURITY REALM",
                                User.class
                        )
                )
        );
    }

}
