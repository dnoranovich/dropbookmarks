/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.udemy.dropbookmarks;

import io.dropwizard.Application;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;

/**
 * An integrationm test for the application, which checks how configuration and
 * authentication work together.
 *
 * @author Dmitry Noranovich
 */
public class AuthIntegrationTest {

    /**
     * The path to the test configuration file. The file is in the
     * src/test/resources folder.
     */
    private static final String CONFIG_PATH
            = ResourceHelpers.resourceFilePath("test-config.yml");

    /**
     * The class rule to start the application before all test methods.
     */
    @ClassRule
    public static final DropwizardAppRule<DropBookmarksConfiguration> RULE
            = new DropwizardAppRule<>(DropBookmarksApplication.class,
                    CONFIG_PATH);

    /**
     * This field is necessary for authentication purposes and provides user
     * credentials when required.
     */
    private static final HttpAuthenticationFeature FEATURE
            = HttpAuthenticationFeature.basic("udemy", "p@ssw0rd");

    /**
     * A part of resource URI including protocol, host and port.
     */
    private static final String TARGET
            = "https://localhost:8443";

    /**
     * The path to the secured method.
     */
    private static final String PATH
            = "/hello/secured";

    /**
     * The pass to the key store which is necessary for HTTPS support.
     */
    private static final String TRUST_STORE_FILE_NAME
            = "dropbookmarks.keystore";

    /**
     * The password of the keystore.
     */
    private static final String TRUST_STORE_PASSWORD
            = "p@ssw0rd";

    /**
     * A client to access resources.
     */
    private Client client;

    /**
     * Initializations before test methods.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        Application<DropBookmarksConfiguration> application
                = RULE.getApplication();
        application.run("db", "migrate", "-i DEV", CONFIG_PATH);
    }

    /**
     * Initializations before each test method.
     */
    @Before
    public void setUp() {
        SslConfigurator configurator
                = SslConfigurator.newInstance();
        configurator.trustStoreFile(TRUST_STORE_FILE_NAME)
                .trustStorePassword(TRUST_STORE_PASSWORD);
        SSLContext context = configurator.createSSLContext();
        client = ClientBuilder.newBuilder()
                .sslContext(context)
                .build();
    }

    /**
     * Clean up before each test method.
     */
    @After
    public void tearDown() {
        client.close();
    }

    /**
     * Tests the case when no credentials are provided.
     */
    @Test
    public void testSadPath() {
        Response response = client
                .target(TARGET)
                .path(PATH)
                .request()
                .get();

        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(),
                response.getStatus());
    }

    /**
     * Tests the case when credentials are provided.
     */
    @Test
    public void testHappyPath() {
        String expected = "Hello secured world";
        client.register(FEATURE);
        String actual = client
                .target(TARGET)
                .path(PATH)
                .request(MediaType.TEXT_PLAIN)
                .get(String.class);

        assertEquals(expected, actual);
    }

}
